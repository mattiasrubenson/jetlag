from bot import client, token
from db import create_db_and_tables, read_starting_data
import os
from pathlib import Path


def reset_game():
    try:
        os.remove(Path("database.db"))
    except FileNotFoundError:
        pass

    with open(Path("data/snapshots.json"), "w") as f:
        f.write("{}")
    create_db_and_tables()
    read_starting_data()
    client.run(token)


reset_game()
