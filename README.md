# JetLag: Skåne

A territory control game inspired by the YouTube channel [Jet Lag: The Game](https://www.youtube.com/@jetlagthegame). The game is designed to be played in teams of two across two game days and all of Skåne.

## Game Overview

The goal of JLS is to amass as many points as possible. Points are gained from controlling towns. Twice an hour the control of each town is checked and their value added to the controlling team's tally.

Game days last between 09:00 and 19:00. The bot will not add points or accept challenges outside of these hours.

To control a town a team must complete a randomly drawn challenge. If the challenge is too hard, the team can either travel to a new town and ignore it, or wait out a 30 minute timer. A team cannot challenge for a town without being there first.

Points can be used to purchase power-ups and to curse other teams.

![img.png](docs/map.png)

## How to play

### 1. Setup
Starting the game is done by running this discord bot. It will then start counting points and accepting challenges at the next `playing_time_start`. Stopping the game is done by stopping the bot.

The bot will regularly update the general channel with the current leaderboard.

![points.png](docs/points.png)

### 2. Creating teams
Creating teams is done with the command `!create_team <team_name>` in the discord channel. Each team automatically gets their own private channel to post in.

![create_team.png](docs/create_team.png)

![create_team2.png](docs/create_team2.png)

### 3. Creating players/joining teams
A discord user becomes a player by joining a team with the command `!join_team <team_name>`.

![join_team.png](docs/join_team.png)

The player will then be added to the team's private channel.

### 4. Challenging for a town
Teams challenge for a town by a team member using the command `!challenge <town_name>` in their team channel. The bot will then post a random sample of up to three challenges for the team to choose from.

![img.png](docs/challenge.png)

Choose a challenge by clicking its button.

![challenge2.png](docs/challenge2.png)

The team gets 30 minutes to complete the challenge and claim the town with no other team being able to challenge for the same town.

### 5. Claiming a town
Claiming a town is done by typing the command `!claim <town_name>` and attaching photo/video proof in the team channel.

![claim.png](docs/claim.png)

The bot will then post proof in the general channel for all teams to see.

![claim2.png](docs/claim2.png)

### 6. Purchasing and using power-ups/curses

Power-ups and curses work similarly. They are bought with the command `!powerup`. This command will trigger a list of powerups to choose from. 

![powerup1.png](docs/powerup1.png)

![powerup2.png](docs/powerup2.png)

Choosing a powerup presents you with a form where you can make choices for the powerup.

![docs/powerup3.png](docs/powerup3.png)

The bot posts in the general challenge after a power-up has been used.

![docs/powerup4.png](docs/powerup4.png)

## Setting up your own game

Defining a new game is simple: swap out the challenges in data/challenges.json and the towns in data/towns.json. If you want new curses/power-ups, these are created in powerups.py and curses.py respectively, but require more work to create and edit.

## Configuration

Configuration is done in the config.py file. Fill it out according to the comments in the file.

