import discord
import asyncio
import game
import random
from discord.ext import commands
import discord.ui
from table2ascii import table2ascii as t2a, PresetStyle
from config import application_id, token, public_key, permissions_integer, playing_hours_end, playing_hours_start, round_timer
from powerups import powerups
from curses import curses
import datetime


intents = discord.Intents.default()
intents.message_content = True


class MyClient(commands.Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(command_prefix='!', application_id=application_id, intents=intents)

    async def on_ready(self):
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        print('------')
        channel = self.get_channel(1232348409114001431)
        categories = [c.name for c in channel.guild.categories]
        if "teams" not in categories:
            self.teams_category = await channel.guild.create_category(name="teams")
        else:
            self.teams_category = discord.utils.get(self.get_all_channels(), name="teams")

    async def setup_hook(self) -> None:
        # create the background task and run it in the background
        self.bg_task = self.loop.create_task(self.update_points())
        await self.tree.sync()

    async def update_points(self):
        await self.wait_until_ready()
        channel = self.get_channel(1232348409114001431)
        while not self.is_closed():
            if datetime.time.fromisoformat(playing_hours_start) <= datetime.datetime.now().time() <= datetime.time.fromisoformat(playing_hours_end):
                await game.update_points()
                teams = await game.select_teams()
                teams = sorted(teams, key=lambda team: team.points)

                output = t2a(
                    header=["Team", "Points"],
                    body=[[team.name, team.points] for team in teams],
                    column_widths=[None, 8],
                    style=PresetStyle.thin_compact
                )
                await channel.send(f"**Current leaderboard**\n```{output}```")
            await asyncio.sleep(60*round_timer)


client = MyClient(intents=intents)


@client.command()
async def reset(ctx):
    from app import reset_game
    for team in await game.select_teams():
        # TODO Check if this removes team channels correctly
        print(team)
        print(ctx.guild.channels)
        await ctx.guild.get_channel(team.discord_channel_id).delete()
    reset_game()


@client.command()
async def create_team(ctx, name: str):
    overwrites = {
        ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False),
        ctx.guild.me: discord.PermissionOverwrite(read_messages=True)
    }
    channel = await ctx.guild.create_text_channel(name, overwrites=overwrites, category=client.teams_category)
    await game.create_team(name, channel.id)
    await ctx.send(f"Created team **{name}**")


@client.command()
async def join_team(ctx, name: str):
    channel_id = await game.join_team(name=name, player_id=ctx.author.id, player_name=ctx.author.name)
    channel = ctx.guild.get_channel(channel_id)

    overwrites = {
        ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False),
        ctx.author: discord.PermissionOverwrite(read_messages=True)
    }

    for member in channel.members:
        overwrites[member] = discord.PermissionOverwrite(read_messages=True)

    await channel.edit(overwrites=overwrites)

    await ctx.send(f'**{ctx.author.name}** joined team **{name}**')


class ChallengeButton(discord.ui.Button):
    def __init__(self, team_name: str, town_name: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.team_name = team_name
        self.town_name = town_name

    async def callback(self, interaction: discord.Interaction):
        await interaction.response.send_message(f"Challenge **{self.label}** was chosen!")
        await interaction.guild.get_channel(1232348409114001431).send(f"Team **{self.team_name}** is challenging for town **{self.town_name}**")
        self.disabled = True
        self.view.stop()


@client.command()
async def challenge(ctx, town_name: str):
    # check so that the time is not outside playing time
    if not datetime.time.fromisoformat(playing_hours_start) <= datetime.datetime.now().time() <= datetime.time.fromisoformat(playing_hours_end):
        return
    # get player's team
    team = await game.get_player_team(ctx.author.id)
    # get list of valid challenges for town
    challenges = await game.get_town_challenges(town_name)
    if not challenges:
        await ctx.send("Town already under challenge.")
        return
    if not await game.challenge_town(town_name, team.discord_channel_id):
        await ctx.send("Town already controlled by you.")
        return
    team_channel = ctx.guild.get_channel(team.discord_channel_id)
    # choose a challenge from three random available ones
    view = discord.ui.View()
    styles = [discord.ButtonStyle.green, discord.ButtonStyle.red, discord.ButtonStyle.blurple]
    random_challenges = random.sample(challenges, k=min(len(challenges), 2))
    for i in range(len(random_challenges)):
        view.add_item(ChallengeButton(label=random_challenges[i].name,
                                      style=styles[i],
                                      custom_id=str(random_challenges[i].id),
                                      team_name=team.name,
                                      town_name=town_name))
    description_text = ""
    for challenge in random_challenges:
        description_text += f"**{challenge.name}**: {challenge.text}\n"
    await team_channel.send("",
                            view=view,
                            embed=discord.Embed(title=f"**Available challenges for {town_name}**",
                                                description=description_text))
    # Countdown the available time of 30 minutes
    await asyncio.sleep(30*60)
    # If claim has been successful, town will not be under challenge
    if not await game.get_town_challenges(town_name):
        await game.change_town_challenge_status(town_name, False)


@client.command()
async def claim(ctx, town_name: str):
    if not datetime.time.fromisoformat(playing_hours_start) <= datetime.datetime.now().time() <= datetime.time.fromisoformat(playing_hours_end):
        return
    # get player's team
    team = await game.get_player_team(ctx.author.id)
    # claim completed challenge
    embed = discord.Embed(title="Proof")
    embed.set_image(url=ctx.message.attachments[0].url)
    await ctx.guild.get_channel(1232348409114001431).send(f"Team **{team.name}** claimed town **{town_name}**", embed=embed)
    # TODO Implement a requirement for a member of any other team to accept the proof?
    # change ownership of town
    await game.claim_town(town_name, team.id)


class PowerupSelect(discord.ui.Select):
    def __init__(self):
        # A list comprehension creating options from a list of powerups
        # TODO This does not display the full descriptions - they should probably be in an embed
        options = [discord.SelectOption(label=powerups[p].model_fields['name'].default,
                                        description=powerups[p].model_fields['description'].default) for p in powerups]
        super().__init__(placeholder='Choose a power-up...', min_values=1, max_values=1, options=options)

    async def callback(self, interaction: discord.Interaction):
        # The chosen powerup name
        chosen_powerup = powerups[self.values[0]]
        # Present a modal with the information needed to perform the powerup (i.e. town name etc)
        await interaction.response.send_modal(PowerupOptions(chosen_powerup))


class PowerupOptions(discord.ui.Modal):
    def __init__(self, powerup):
        self.powerup = powerup
        title = f"Options for {powerup.model_fields['name'].default}"
        super().__init__(title=title)
        import inspect
        fields = inspect.signature(powerup.model_fields['effect'].default).parameters
        self.fields = fields
        for field in fields.keys():
            self.add_item(discord.ui.TextInput(label=fields[field].name,
                                               placeholder=str(fields[field].annotation)))

    async def on_submit(self, interaction: discord.Interaction):
        await interaction.response.send_message(f'Chosen power-up: **{self.powerup.model_fields["name"].default}**')
        # Await the effect of the powerup
        kwargs = {c.label: c.value for c in self.children}
        p = self.powerup(player_discord_id=interaction.user.id)
        try:
            await p.model_async_validate()
        except ValueError:
            await interaction.channel.send(f'Not enough points to use chosen power-up')
            return
        arguments = '\n' + '\n'.join([f"{k} = {kwargs[k]}" for k in kwargs])
        await asyncio.gather(
            p.effect(**kwargs),
            interaction.guild.get_channel(1232348409114001431).send(f"Team **{(await game.get_player_team(p.player_discord_id)).name}** has used powerup **{p.name}** with arguments:\n {arguments}")
        )


class PowerupView(discord.ui.View):
    def __init__(self):
        super().__init__()
        self.add_item(PowerupSelect())


@client.command()
async def powerup(ctx):
    await ctx.send('**Available power-ups**', view=PowerupView())


class CurseSelect(discord.ui.Select):
    def __init__(self):
        # A list comprehension creating options from a list of curses
        # TODO This does not display the full descriptions - they should probably be in an embed
        options = [discord.SelectOption(label=curses[p].model_fields['name'].default,
                                        description=curses[p].model_fields['description'].default) for p in curses]
        super().__init__(placeholder='Choose a curse...', min_values=1, max_values=1, options=options)

    async def callback(self, interaction: discord.Interaction):
        # The chosen curse name
        chosen_curse = curses[self.values[0]]
        # Present a modal with the information needed to perform the curse (i.e. town name etc)
        await interaction.response.send_modal(CurseOptions(chosen_curse))


class CurseOptions(discord.ui.Modal):
    def __init__(self, curse):
        self.curse = curse
        title = f"Options for {curse.model_fields['name'].default}"
        super().__init__(title=title)
        import inspect
        fields = inspect.signature(curse.model_fields['effect'].default).parameters
        self.fields = fields
        for field in fields.keys():
            self.add_item(discord.ui.TextInput(label=fields[field].name,
                                               placeholder=str(fields[field].annotation)))

    async def on_submit(self, interaction: discord.Interaction):
        await interaction.response.send_message(f'Chosen curse: **{self.curse.model_fields["name"].default}**')
        # Await the effect of the powerup
        kwargs = {c.label: c.value for c in self.children}
        p = self.curse(player_discord_id=interaction.user.id)
        try:
            await p.model_async_validate()
        except ValueError:
            await interaction.channel.send(f'Not enough points to use chosen curse')
            return
        await p.effect(**kwargs)
        arguments = '\n' + '\n'.join([f"{k} = {kwargs[k]}" for k in kwargs])
        await interaction.guild.get_channel(1232348409114001431).send(f"Team {(await game.get_player_team(p.player_discord_id)).name} has used curse **{p.name}** with arguments {arguments}")

    async def on_error(self, interaction: discord.Interaction, error: Exception) -> None:
        await interaction.response.send_message('Oops! Something went wrong.', ephemeral=True)


class CurseView(discord.ui.View):
    def __init__(self):
        super().__init__()
        self.add_item(CurseSelect())


@client.command()
async def curse(ctx):
    await ctx.send('**Available curses**', view=CurseView())


# TODO Check location stuff?
# https://github.com/bilde2910/Hauk
# https://github.com/Taraal/LocationBot