from sqlmodel import Field, SQLModel, create_engine, Relationship, Session, select
from sqlalchemy.orm import selectinload


class TownChallengeLink(SQLModel, table=True):
    town_id: int | None = Field(default=None, foreign_key="town.id", primary_key=True)
    challenge_id: int | None = Field(default=None, foreign_key="challenge.id", primary_key=True)


class TeamChallengeLink(SQLModel, table=True):
    team_id: int | None = Field(default=None, foreign_key="team.id", primary_key=True)
    challenge_id: int | None = Field(default=None, foreign_key="challenge.id", primary_key=True)


class Team(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    name: str
    points: int = 0
    discord_channel_id: int
    players: list["Player"] = Relationship(back_populates="team")
    towns: list["Town"] = Relationship(back_populates="controller")
    challenges: list["Challenge"] = Relationship(back_populates="teams", link_model=TeamChallengeLink)


class Town(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    name: str
    value: int
    under_challenge: bool = False

    controller: Team | None = Relationship(back_populates="towns")
    controller_id: int | None = Field(default=None, foreign_key="team.id")
    challenges: list["Challenge"] = Relationship(back_populates="towns", link_model=TownChallengeLink)


class Player(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    name: str
    discord_id: int

    team: Team | None = Relationship(back_populates="players")
    team_id: int | None = Field(default=None, foreign_key="team.id")


class Challenge(SQLModel, table=True):
    id: int | None = Field(default=None, primary_key=True)
    name: str
    text: str
    towns: list["Town"] = Relationship(back_populates="challenges", link_model=TownChallengeLink)
    teams: list["Team"] = Relationship(back_populates="challenges", link_model=TeamChallengeLink)


sqlite_url = f"sqlite:///database.db"
engine = create_engine(sqlite_url, echo=False) # For debug set echo to true


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def read_starting_data():
    import json
    from pathlib import Path
    with open(Path("data/towns.json"), encoding="utf-8") as f:
        towns = json.load(f)
        with Session(engine) as session:
            for town in towns:
                session.add(Town(name=town, value=towns[town]))
            session.commit()

    with open(Path("data/challenges.json"), encoding="utf-8") as f:
        challenges = json.load(f)
        with Session(engine) as session:
            results = session.exec(select(Town))
            towns = results.all()
            for challenge in challenges:
                c = Challenge(name=challenge, text=challenges[challenge]['text'])
                for town in towns:
                    if town.name in challenges[challenge]['towns']:
                        c.towns.append(town)
                session.add(c)
            session.commit()