import asyncio

from pydantic import BaseModel, model_validator
from pydantic.fields import PydanticUndefined
from pydantic_async_validation import async_model_validator, AsyncValidationModelMixin

from typing import Self, Callable
from game import change_town_challenge_status, get_player_team, remove_points


class Curse(AsyncValidationModelMixin, BaseModel):
    cost: int
    name: str
    description: str
    player_discord_id: int
    effect: Callable

    @classmethod
    async def get_defaults(cls) -> dict:
        fields = cls.model_fields
        return {f: fields[f].default for f in fields if fields[f].default is not PydanticUndefined}

    @async_model_validator(mode='after')
    async def check_points(self) -> Self:
        # Get the team of the player
        team = await get_player_team(self.player_discord_id)
        # Validate that the team has enough money
        if team.points < self.cost:
            raise ValueError('Not enough points')
        # Get the team of the player
        team = await get_player_team(self.player_discord_id)
        # Withdraw the money
        await remove_points(team_id=team.points, points=self.cost)
        return self


async def walk_sideways(team_name: str):
    pass


class WalkSideways(Curse):
    cost: int = 75
    name: str = "Crabwalk"
    description: str = "All members of the cursed team must only walk sideways for the next 45 minutes."
    effect: Callable = walk_sideways


curses = {WalkSideways.model_fields['name'].default: WalkSideways}
