import asyncio

from pydantic import BaseModel, model_validator
from pydantic.fields import PydanticUndefined
from pydantic_async_validation import async_model_validator, AsyncValidationModelMixin
from game import change_town_challenge_status, get_player_team, remove_points, challenge_town
from typing import Self, Callable


class Powerup(AsyncValidationModelMixin, BaseModel):
    cost: int
    name: str
    description: str
    player_discord_id: int
    effect: Callable

    @classmethod
    async def get_defaults(cls) -> dict:
        fields = cls.model_fields
        return {f: fields[f].default for f in fields if fields[f].default is not PydanticUndefined}

    @async_model_validator(mode='after')
    async def check_points(self) -> Self:
        # Get the team of the player
        team = await get_player_team(self.player_discord_id)
        # Validate that the team has enough money
        if team.points < self.cost:
            raise ValueError('Not enough points')
        # Get the team of the player
        team = await get_player_team(self.player_discord_id)
        # Withdraw the money
        await remove_points(team_id=team.id, points=self.cost)
        return self


async def protect_town(town_name: str):
    # Protect the town
    await change_town_challenge_status(town_name=town_name, status=True)
    # Wait for two hours
    await asyncio.sleep(120)
    # Unprotect the town
    await change_town_challenge_status(town_name=town_name, status=False)


class ProtectTown(Powerup):
    cost: int = 100
    name: str = "Protect a town"
    description: str = "Protect a town from challenges for two hours."
    effect: Callable = protect_town


class ChallengeAnyTown(Powerup):
    cost: int = 400
    name: str = "Challenge any town"
    description: str = "Immediately challenge any town on the map."
    effect: Callable = challenge_town


powerups = {ProtectTown.model_fields['name'].default: ProtectTown,
            ChallengeAnyTown.model_fields['name'].default: ChallengeAnyTown}