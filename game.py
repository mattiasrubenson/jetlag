from sqlmodel import Session, select
from sqlalchemy.orm import selectinload
from db import engine, Team, Player, Town
import json
from pathlib import Path
from datetime import datetime


async def create_team(name: str, channel_id: int):
    with Session(engine) as session:
        session.add(Team(name=name, discord_channel_id=channel_id))
        session.commit()


async def create_player(name: str, discord_id: int):
    with Session(engine) as session:
        session.add(Player(name=name, discord_id=discord_id))
        session.commit()


async def select_teams():
    with Session(engine) as session:
        statement = select(Team).options(selectinload(Team.players))
        results = session.exec(statement)
        teams = results.all()
    return teams


async def update_points():
    with Session(engine) as session:
        statement = select(Team)
        results = session.exec(statement)
        teams = results.all()
        for team in teams:
            for town in team.towns:
                team.points += town.value
            session.add(team)
        session.commit()
    with open(Path("data/snapshots.json"), encoding="utf-8") as f:
        snapshots = json.load(f)
    with Session(engine) as session:
        teams = session.exec(select(Team)).all()
        towns = session.exec(select(Town).options(selectinload(Town.controller))).all()
    snapshot = {
        "teams": {team.name: team.points for team in teams},
        "towns": {town.name: town.controller.name if town.controller else None for town in towns }
    }
    snapshots[datetime.now().isoformat()] = snapshot
    with open(Path("data/snapshots.json"), "w", encoding="utf-8") as f:
        json.dump(snapshots, f, ensure_ascii=False)


async def join_team(name: str, player_id: int, player_name: str) -> int:
    with Session(engine) as session:
        results = session.exec(select(Team).where(Team.name == name))
        team = results.first()
        results = session.exec(select(Player).where(Player.discord_id == player_id))
        player = results.first()
        if player is None:
            await create_player(name=player_name, discord_id=player_id)
            results = session.exec(select(Player).where(Player.discord_id == player_id))
            player = results.first()
        player.team = team
        channel_id = team.discord_channel_id
        session.add(player)
        session.commit()
    return channel_id


async def get_player_team(player_id: int):
    with Session(engine) as session:
        statement = select(Player).where(Player.discord_id == player_id)
        results = session.exec(statement)
        player = results.first()
        team = player.team
    return team


async def get_town_challenges(town_name: str):
    with Session(engine) as session:
        results = session.exec(select(Town).where(Town.name == town_name))
        town = results.first()
        if town.under_challenge:
            return None
        challenges = town.challenges
    return challenges


async def challenge_town(town_name: str, team_discord_id: int):
    with Session(engine) as session:
        results = session.exec(select(Town).where(Town.name == town_name))
        town = results.first()
        try:
            if town.controller.discord_channel_id == team_discord_id:
                return False
        except AttributeError:
            pass
        town.under_challenge = True
        session.add(town)
        session.commit()
    return True


async def get_town_challenge_status(town_name: str):
    with Session(engine) as session:
        results = session.exec(select(Town).where(Town.name == town_name))
        town = results.first()
    return town.under_challenge


async def change_town_challenge_status(town_name: str, status: bool):
    with Session(engine) as session:
        results = session.exec(select(Town).where(Town.name == town_name))
        town = results.first()
        town.under_challenge = status
        session.add(town)
        session.commit()


async def claim_town(town_name: str, team_id: int):
    with Session(engine) as session:
        town = session.exec(select(Town).where(Town.name == town_name)).first()
        town.under_challenge = False
        team = session.exec(select(Team).where(Team.id == team_id)).first()
        town.controller = team
        session.add(town)
        session.add(team)
        session.commit()


async def remove_points(team_id: int, points: int):
    with Session(engine) as session:
        team = session.exec(select(Team).where(Team.id == team_id)).first()
        team.points -= points
        session.add(team)
        session.commit()
