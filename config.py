## THESE ARE FOR YOUR DISCORD BOT ##
application_id = "" 
public_key = ""
permissions_integer = 0
token = ""

## THESE ARE FOR THE GAME
playing_hours_start = "09:00" # This is the time at which every game day starts, local time
playing_hours_end = "19:00" # This is the time at which every game day ends, local time
round_timer = 30 # This is the amount of minutes between each instance of point generation